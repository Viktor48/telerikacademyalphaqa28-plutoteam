# Pluto Team

## Table of contents
1. [Project introduction](#project_introduction)
2. [System under test description](#description)
3. [Team](#team)
4. [Test tools](#test_tools)
5. [Deliverables](#deliverables)
6. [Running automation scripts](#running_automation_scripts)

## Project introduction <a name= "project_introduction"></a>
This project is part of our study curriculum in the Telerik Academy Alpha QA track. It is the final project before graduation. In it, we put everything we know into practice to show our progress.


## System under test introduction <a name="description"></a>
The WEare Social Network is a web application that enables people looking for new career opportunities to connect with each other and with employers.

**The system main functionality**
 - to  connect with other people; 
 - to create, comment on and like posts about new jobs
## QA team members <a name="team"></a>
- Viktor Varbanov
- Valentin Bogdanov


## Test tools <a name="test_tools"></a>
- Source code management - GitLab
- Bug tracking tool - GitLab
- Api - Postman
- UI testing  - Java + Selenium WebDriver + POM
- Schedule management tool - Trello board
- Test framework - Junit 5

## Deliverables <a name = "deliverables"></a>
 - <a href="https://telerikacademy-my.sharepoint.com/:w:/p/valentin_bogdanov_a28_learn/EdK0acncV1NEoC6Gqam_FNABt2r52TvkQnNQUX-q-KpcPA?e=4o9ha7">Test plan</a>
 - <a href="https://telerikacademy-my.sharepoint.com/:x:/p/valentin_bogdanov_a28_learn/Ed0zcUMO6H5GiShQ4Cub4GkBiaoSekv9lNCMwm9ehfaGyQ?e=oCtR8x">Test cases</a>
 - <a href="https://telerikacademy-my.sharepoint.com/:x:/p/valentin_bogdanov_a28_learn/Ecdhoi7tZXFNpORqJVeYl0sBs39Xxl5co2fJ88WHZq7bvQ?e=VJ33SZ">Test report</a>
 - <a href="https://telerikacademy-my.sharepoint.com/:x:/p/valentin_bogdanov_a28_learn/ER60Lz2qNZJNmbuaKQKZSRwBNbLDRLCCwL2ikyaoKgX6mw?e=WH5Mjf">Exploratory Testing Report</a>
 - <a href="https://telerikacademy-my.sharepoint.com/:u:/p/valentin_bogdanov_a28_learn/EbXKCvn3nOhGnnhq7qMcf0gBngP3QLV8xJi5w6VFXT1IrA?e=qy48fj">XMind scheme</a>
 - <a href="https://trello.com/b/I0vw8KQC/final-project-of-the-pluto-group">QA Project Management Board - Trello</a>
 - <a href="https://telerikacademy-my.sharepoint.com/:w:/p/valentin_bogdanov_a28_learn/EfG67eEdcMhMjlr3phryWLwB3_NAtQxy0NUOS5M8yN_QVQ?e=Xepw03">Final Test Report</a>
  - <a href="https://telerikacademy-my.sharepoint.com/:w:/p/valentin_bogdanov_a28_learn/EXrXZxWw9EhHupD4mZDWpS0B7fYebjjhfHFeJTmQwo3oUw?e=baO6GC">Prerequisites for starting the project</a>



## Running automation scripts <a name= "running_automation_scripts"></a>
**Required software and programs:**
- Windows 10 operating system
- JDK 11
- Java 1.8
- Docker 
- Docker Desktop WSL 2 backend
- IntelliJ IDEA Community edition
- Postman - v.8.8.0 or later
- Apache Maven v.3.8.1
- Firefox
- Google Chrome
- Node.js v.14.17.1 or later
- npm v.6.14.13 or later
- Newman v.5.2.4
- Newman HTML Extra Reporter latest version
  
**Execution of the scripts**
- clone the repo on your computer
- run .bat file in each folder
